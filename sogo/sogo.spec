%global sogo_user sogo
%global sogo_major_version 4

# ************************ main package ************************************** #
Summary: SOGo groupware server built around OpenGroupware.org and SOPE
Name: sogo
Version: 4.0.4
Release: 1%{?dist}
# The entire source code is GPLv2+
# except SOPE/*, Attic/OpenChange/RTFHandler*, Main/(MainUIProduct.m|sogo.*|sogod.*)
# and parts of SoObjects/*, UI/*, which are LGPLv2+
License: GPLv2+ and LGPLv2+
URL: https://sogo.nu/
# *** sources *** #
Source0: https://sogo.nu/files/downloads/SOGo/Sources/SOGo-%{version}.tar.gz
Source1: sogo-timer
Source2: sogo-backup.service
Source3: sogo-backup.timer
Source4: sogo-email-alarms.service
Source5: sogo-email-alarms.timer
Source6: sogo-expire-session.service
Source7: sogo-expire-session.timer
Source8: sogo-tmpwatch.service
Source9: sogo-tmpwatch.timer
Source10: sogo-update-autoreply.service
Source11: sogo-update-autoreply.timer
# *** patches *** #
# replace hardcoded paths with RPM macros
Patch0: rpm-path-macros.patch
# replace all non-specific python shebangs with /usr/bin/python2
Patch1: python2.patch
# fix location of docbook.xsl in documentation makefile
Patch2: docbook-xsl.patch
# patch Scripts/(sogo-backup.sh|tmpwatch) to respect environment variables
#Patch3: script-environment.patch
# *** dependencies *** #
# general
Requires: gnustep-base libmemcached memcached zip lasso libcurl
# SOPE library dependencies
Requires: sope-core = %{version} sope-appserver = %{version} sope-ldap = %{version} sope-sbjson = %{version}
Requires: sope-cards%{?_isa} = %{version}-%{release} sope-gdl1-contentstore%{?_isa} = %{version}-%{release}
# to create SOGo user and group
Requires(pre): shadow-utils
# GNUstep and Objective-C
BuildRequires: gcc gcc-objc gnustep-make gnustep-base gnustep-base-devel
# system shared libraries
BuildRequires: python2 openssl-devel libmemcached-devel lasso-devel libcurl-devel openldap-devel
# documentation
BuildRequires: asciidoc fop
# SOPE shared libraries
BuildRequires: sope-appserver-devel = %{version} sope-core-devel = %{version} sope-ldap-devel = %{version}
BuildRequires: sope-mime-devel = %{version} sope-sbjson-devel = %{version} sope-gdl1-devel = %{version}
BuildRequires: sope-xml-devel = %{version}

%description
A groupware server built around OpenGroupware.org (OGo) and
the SOPE application server, focused on scalability.

The Inverse edition of this project has many feature enhancements:
- CalDAV and GroupDAV compliance
- full handling of vCard as well as vCalendar/iCalendar formats
- support for folder sharing and ACLs

The Web interface has been rewritten in an AJAX fashion to provided a faster
UI for the users, consistency in look and feel with the Mozilla applications,
and to reduce the load of the transactions on the server.

# ****************************** subpackages ************************** #
%package tool
Summary:      Command-line toolsuite for SOGo
Requires:     %{name} = %{version}
%description tool
Administrative tool for SOGo that provides the following internal commands:
  backup          -- backup user folders
  restore         -- restore user folders
  remove-doubles  -- remove duplicate contacts from the user addressbooks
  check-doubles   -- list user addressbooks with duplicate contacts

%package slapd-sockd
Summary:      SOGo backend for slapd and back-sock
%description slapd-sockd
SOGo backend for slapd and back-sock, enabling access to private addressbooks
via LDAP.

%package ealarms-notify
Summary:      SOGo utility for executing email alarms
%description ealarms-notify
SOGo utility executed each minute via a cronjob for executing email alarms.

%package activesync
Summary:      SOGo module to handle ActiveSync requests
Requires:     libwbxml, %{name} = %{version}
BuildRequires: libwbxml-devel
%description activesync
SOGo module to handle ActiveSync requests

%package devel
Summary:      Development headers and libraries for SOGo
%description devel
Development headers and libraries for SOGo. Needed to create modules.

%package -n sope-gdl1-contentstore
Summary:      Storage backend for folder abstraction.
Requires:     sope-gdl1 = %{version}
%description -n sope-gdl1-contentstore
The storage backend implements the "low level" folder abstraction, which is
basically an arbitary "BLOB" containing some document.

SOPE is a framework for developing web applications and services. The
name "SOPE" (SKYRiX Object Publishing Environment) is inspired by ZOPE.

%package -n sope-gdl1-contentstore-devel
Summary:      Development files for the GNUstep database libraries
Requires:     sope-gdl1 = %{version}
%description -n sope-gdl1-contentstore-devel
This package contains the header files for SOPE\'s GDLContentStore library.

SOPE is a framework for developing web applications and services. The
name "SOPE" (SKYRiX Object Publishing Environment) is inspired by ZOPE.

%package -n sope-cards
Summary:      SOPE versit parsing library for iCal and VCard formats
%description -n sope-cards
SOPE versit parsing library for iCal and VCard formats

%package -n sope-cards-devel
Summary:      SOPE versit parsing library for iCal and VCard formats
Requires:     sope-cards%{?_isa} = %{version}-%{release}
%description -n sope-cards-devel
SOPE versit parsing library for iCal and VCard formats

# ****************************** preparation **************************
%prep
%autosetup -p1 -n SOGo-%{version}

# replace macro placeholder in config files & scripts
FILES="Scripts/%{name}-systemd-redhat
Scripts/%{name}-systemd.conf
Scripts/%{name}.conf
Scripts/tmpwatch
Scripts/logrotate
Scripts/fail2ban/%{name}-jail.local
Scripts/fail2ban/%{name}-filter.conf
Scripts/%{name}-default
Scripts/%{name}-backup.sh
Scripts/sql-update-2.2.17_to_2.3.0.sh
Scripts/sql-update-2.2.17_to_2.3.0-mysql.sh
Scripts/sql-update-3.0.0-to-combined-mysql.sh
Scripts/sql-update-3.0.0-to-combined.sh
Scripts/sql-update-3.2.10_to_4.0.0.sh
Scripts/sql-update-3.2.10_to_4.0.0-mysql.sh
Documentation/SOGoInstallationGuide.asciidoc
Documentation/SOGoDevelopersGuide.asciidoc
Attic/SOGoNativeOutlookConfigurationGuide.asciidoc
Attic/OpenChange/GNUmakefile
Attic/OpenChange/MAPIStoreMIME.m
SoObjects/SOGo/SOGoSystemDefaults.m
SoObjects/SOGo/NSData+Crypto.m
SoObjects/SOGo/SOGoLDAPUserDefaults.m
SoObjects/SOGo/SOGoDefaults.plist
Tools/SOGoToolDumpDefaults.m
Tools/SOGoSockD.m
Apache/SOGo.conf"

for FILE in $FILES; do
  sed -i -e "s|%%{_rundir}|%{_rundir}|g" \
         -e "s|%%{_sysconfdir}|%{_sysconfdir}|g" \
         -e "s|%%{_bindir}|%{_bindir}|g" \
         -e "s|%%{_sbindir}|%{_sbindir}|g" \
         -e "s|%%{_localstatedir}|%{_localstatedir}|g" \
         -e "s|%%{_sharedstatedir}|%{_sharedstatedir}|g" \
         -e "s|%%{gnustep_libdir}|%{gnustep_libdir}|g" \
         -e "s|%%{sogo_user}|%{sogo_user}|g" \
         -e "s|%%{name}|%{name}|g" \
         $FILE
done

# ****************************** build ********************************
%build
%set_build_flags
# no paths specified, as GNUStep takes care of the proper install
# directories
./configure --disable-debug --enable-saml2

# make program
%{make_build}

# make documentation
(cd Documentation; %{make_build})

# ****************************** install ******************************
%install
QA_SKIP_BUILD_ROOT=1
export QA_SKIP_BUILD_ROOT

# install main program
%{make_install} GNUSTEP_INSTALLATION_DOMAIN=SYSTEM

# for some reason, stuff in the ActiveSync subdir does not get
# installed automatically
(cd ActiveSync; \
%{make_install} GNUSTEP_INSTALLATION_DOMAIN=SYSTEM )

# create directories in buildroot
install -d -m 0755 %{buildroot}/%{_unitdir}/
install -d -m 0755 %{buildroot}/%{_tmpfilesdir}/
install -d -m 0755 %{buildroot}/%{_libexecdir}/%{name}/
install -d -m 0755 %{buildroot}/%{_sysconfdir}/logrotate.d/
install -d -m 0755 %{buildroot}/%{_sysconfdir}/sysconfig/
install -d -m 0750 %{buildroot}/%{_sysconfdir}/%{name}/
install -d -m 0755 %{buildroot}/%{_sharedstatedir}/%{name}/
install -d -m 0755 %{buildroot}/%{_localstatedir}/log/%{name}/
install -d -m 0755 %{buildroot}/%{_localstatedir}/spool/%{name}/
install -d -m 0755 %{buildroot}/%{_rundir}/%{name}/
# install files
install -m 0640 Scripts/%{name}.conf %{buildroot}/%{_sysconfdir}/%{name}/
#cat Apache/SOGo.conf | sed -e "s@/lib/@/%%{_lib}/@g" > %%{buildroot}/%%{_sysconfdir}/httpd/conf.d/SOGo.conf
install -m 0755 Scripts/tmpwatch %{buildroot}/%{_libexecdir}/%{name}/%{name}-tmpwatch
install -m 0755 Scripts/%{name}-backup.sh %{buildroot}/%{_libexecdir}/%{name}/%{name}-backup
install -m 0640 Scripts/logrotate %{buildroot}/%{_sysconfdir}/logrotate.d/%{name}
install -m 0644 Scripts/%{name}-systemd-redhat %{buildroot}/%{_unitdir}/sogod.service
install -m 0644 Scripts/%{name}-systemd.conf %{buildroot}/%{_tmpfilesdir}/%{name}.conf
install -m 0644 Scripts/%{name}-default %{buildroot}/%{_sysconfdir}/sysconfig/%{name}
# install package-specific source files
install -m 0644 %{SOURCE1} %{buildroot}/%{_sysconfdir}/sysconfig/
install -m 0644 %{SOURCE2} %{buildroot}/%{_unitdir}/
install -m 0644 %{SOURCE3} %{buildroot}/%{_unitdir}/
install -m 0644 %{SOURCE4} %{buildroot}/%{_unitdir}/
install -m 0644 %{SOURCE5} %{buildroot}/%{_unitdir}/
install -m 0644 %{SOURCE6} %{buildroot}/%{_unitdir}/
install -m 0644 %{SOURCE7} %{buildroot}/%{_unitdir}/
install -m 0644 %{SOURCE8} %{buildroot}/%{_unitdir}/
install -m 0644 %{SOURCE9} %{buildroot}/%{_unitdir}/
install -m 0644 %{SOURCE10} %{buildroot}/%{_unitdir}/
install -m 0644 %{SOURCE11} %{buildroot}/%{_unitdir}/
# remove unneeded files
rm -rf %{buildroot}/%{_bindir}/test_quick_extract


# **************************** pkgscripts *****************************
%pre
getent group %sogo_user >/dev/null || groupadd -r %sogo_user
getent passwd %sogo_user >/dev/null || \
    useradd -r -g %sogo_user -d %{_sharedstatedir}/%{name} -s %{sbindir}/nologin \
    -c "SOGo Groupware daemon user" %sogo_user
exit 0

%post
# update timestamp on imgs,css,js to let apache know the files changed
find %{gnustep_libdir}/SOGo/WebServerResources  -exec touch {} \;
# make shells scripts in documentation directory executable
find %{_docdir}/ -name '*.sh' -exec chmod a+x {} \;

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

/bin/rm -rf %{_rundir}/%{name}
/bin/rm -rf %{_localstatedir}/spool/%{name}
# not removing /var/lib/sogo to keep .GNUstepDefaults and potential backups

# ****************************** files ********************************
%files
%doc ChangeLog NEWS
%doc Scripts Tests Migration Apache
%doc Documentation/*.pdf
%doc Attic/SOGoNativeOutlookConfigurationGuide.asciidoc
%license COPYING.GPL COPYING.LGPL
%{_unitdir}/sogo*.service
%{_unitdir}/sogo*.timer
%{_tmpfilesdir}/%{name}.conf
%{_libexecdir}/%{name}/%{name}-tmpwatch
%{_libexecdir}/%{name}/%{name}-backup
%dir %attr(0700, %sogo_user, %sogo_user) %{_sharedstatedir}/%{name}
%dir %attr(0700, %sogo_user, %sogo_user) %{_localstatedir}/log/%{name}
%dir %attr(0755, %sogo_user, %sogo_user) %{_rundir}/%{name}
%dir %attr(0700, %sogo_user, %sogo_user) %{_localstatedir}/spool/%{name}
%dir %attr(0750, root, %sogo_user) %{_sysconfdir}/%{name}
%{_sbindir}/sogod
%{_libdir}/%{name}/libSOGo.so*
%{_libdir}/%{name}/libSOGoUI.so*
%{gnustep_libdir}/SOGo/AdministrationUI.SOGo
%{gnustep_libdir}/SOGo/Appointments.SOGo
%{gnustep_libdir}/SOGo/CommonUI.SOGo
%{gnustep_libdir}/SOGo/Contacts.SOGo
%{gnustep_libdir}/SOGo/ContactsUI.SOGo
%{gnustep_libdir}/SOGo/MailPartViewers.SOGo
%{gnustep_libdir}/SOGo/Mailer.SOGo
%{gnustep_libdir}/SOGo/MailerUI.SOGo
%{gnustep_libdir}/SOGo/MainUI.SOGo
%{gnustep_libdir}/SOGo/PreferencesUI.SOGo
%{gnustep_libdir}/SOGo/SchedulerUI.SOGo
%{gnustep_libdir}/Frameworks/SOGo.framework/Resources
%{gnustep_libdir}/Frameworks/SOGo.framework/Versions/%{sogo_major_version}/%{name}/libSOGo.so*
%{gnustep_libdir}/Frameworks/SOGo.framework/Versions/%{sogo_major_version}/Resources
%{gnustep_libdir}/Frameworks/SOGo.framework/Versions/Current
%{gnustep_libdir}/SOGo/Templates
%{gnustep_libdir}/SOGo/WebServerResources
%{gnustep_libdir}/OCSTypeModels
%{gnustep_libdir}/WOxElemBuilders-*
%config(noreplace) %attr(0640, root, %sogo_user) %{_sysconfdir}/%{name}/%{name}.conf
%config(noreplace) %{_sysconfdir}/sysconfig/%{name}
%config(noreplace) %{_sysconfdir}/sysconfig/%{name}-timer
%config(noreplace) %{_sysconfdir}/logrotate.d/%{name}

%files tool
%{_sbindir}/%{name}-tool

%files ealarms-notify
%{_sbindir}/%{name}-ealarms-notify

%files slapd-sockd
%{_sbindir}/%{name}-slapd-sockd

%files activesync
%doc ActiveSync/README
%license ActiveSync/LICENSE
%{gnustep_libdir}/SOGo/ActiveSync.SOGo

%files devel
%{_includedir}/SOGo
%{_includedir}/SOGoUI
%{_libdir}/%{name}/libSOGo.so*
%{_libdir}/%{name}/libSOGoUI.so*
%{gnustep_libdir}/Frameworks/SOGo.framework/Headers
%{gnustep_libdir}/Frameworks/SOGo.framework/%{name}/libSOGo.so
%{gnustep_libdir}/Frameworks/SOGo.framework/%{name}/SOGo
%{gnustep_libdir}/Frameworks/SOGo.framework/Versions/%{sogo_major_version}/Headers
%{gnustep_libdir}/Frameworks/SOGo.framework/Versions/%{sogo_major_version}/%{name}/libSOGo.so*
%{gnustep_libdir}/Frameworks/SOGo.framework/Versions/%{sogo_major_version}/%{name}/SOGo

%files -n sope-gdl1-contentstore
%{_libdir}/%{name}/libGDLContentStore*.so*

%files -n sope-gdl1-contentstore-devel
%{_includedir}/GDLContentStore
%{_libdir}/%{name}/libGDLContentStore*.so*

%files -n sope-cards
%{_libdir}/%{name}/libNGCards.so*
%{gnustep_libdir}/SaxDrivers-*
%{gnustep_libdir}/SaxMappings
%{gnustep_libdir}/Libraries/Resources/NGCards

%files -n sope-cards-devel
%{_includedir}/NGCards
%{_libdir}/%{name}/libNGCards.so*

# ********************************* changelog *************************
%changelog
* Tue Nov 13 2018 Christopher Engelhard <ce@lcts.de> 4.0.4-1
- new package built with tito
- adapted from Inverse's RHEL specfile

