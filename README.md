[![COPR build status](http://copr.fedorainfracloud.org/coprs/lcts/sogo/package/sogo/status_image/last_build.png "SOGo - COPR build status")](https://copr.fedorainfracloud.org/coprs/lcts/sogo/) - `sogo`  
[![COPR build status](http://copr.fedorainfracloud.org/coprs/lcts/sogo/package/sope/status_image/last_build.png "SOPE - COPR build status")](https://copr.fedorainfracloud.org/coprs/lcts/sogo/) - `sope`


# sogo-rpm
RPM packaging for SOGo Groupware and SOPE libraries

This is the source repository for my SOGo and SOPE RPM packages. You can clone this repo and build the package yourself, or you can install it
directly from [COPR](https://copr.fedorainfracloud.org/coprs/lcts/sogo/):

```
$ dnf copr enable lcts/sogo
$ dnf install sogo
```
More information can be found here: [lcts/sogo](https://copr.fedorainfracloud.org/coprs/lcts/sogo/)

The package is currently available for the following OS:
 * Fedora 27
 * Fedora 28
 * Fedora 29
 * Fedora rawhide

Packages for CentOS/RHEL are available from Inverse directly.
